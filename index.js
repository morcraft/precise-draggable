/**
 * /**
 * @file Archivo que contiene el módulo precise-draggable
 * @namespace index
 * @module precise-draggable
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 */

const $ = require('jquery');
const _ = require('lodash');
const stageResize = require('stage-resize');
const presentation = require('presentation');
const Draggabilly = require('draggabilly');
const mezr = require('mezr');
/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {
    currentProps: {
        amount: 0,
        enabled: true,
        draggies: [],
        lastDraggable: {}
    },
    init: function(){
        
    },
    setDraggable: function(args){
        if(!args.$target){
            throw new Error("A target is expected.");
        }
        if(!args.$target instanceof jQuery){
            throw new Error("Invalid target. A jQuery element was expected.");
        }
        if(!args.$target.length){
            throw new Error("jQuery target not available in DOM.");
        }
        
        const $stage = module.exports.currentProps.$stage;
        
        if(typeof $stage === 'undefined'){
            throw new Error("No stage has been defined.");
        }
        
        if(!$stage instanceof jQuery || !$stage.length){
            throw new Error("Invalid $stage. A jQuery element existent in DOM is expected.");
        }
        
        const draggie = new Draggabilly(args.$target.get(0), args.options || {}); 
        var options = { draggie: draggie } ;
        if(args.data){
            options.data = args.data;
        }
        module.exports.setDragStartListener(options);
        module.exports.setDragMoveListener(options);
        module.exports.setDragEndListener(options);
        module.exports.currentProps.draggies.push( draggie );
        return draggie;
    },
    setDragStartListener: function(args){
        if(!args.draggie){
            throw new Error("Expected a Draggabilly instance.");
        }

        return args.draggie.on('dragStart', function(){
            const scale = stageResize.currentProps.scale;
            //presentation.currentProps.hammerInstance.options.enable = false;
            module.exports.currentProps.$stage.addClass('grabbing');
            module.exports.currentProps.dragging = true;
            module.exports.currentProps.lastDraggable = {
                draggieInstance: args.draggie,
                $element: args.draggie.$element,
                data: args.data || {}
            };
        });
    },
    setDragMoveListener: function(args){
        if(!args.draggie){
            throw new Error("Expected a Draggabilly instance.");
        }

        return args.draggie.on( 'dragMove', function( event, pointer, moveVector ) {

            const scale = stageResize.currentProps.scale;
            if(event.type === 'mousemove'){
                var coords = {
                    x: event.clientX,
                    y: event.clientY
                }
            }
            else{
                if(event.type === 'touchmove'){
                    if(_.size(event.changedTouches) < 1){
                        throw new Error('Error while recognizing touch expression.');
                    }

                    var touchEvent = event.changedTouches[0];
                    if(typeof touchEvent === 'undefined'){
                        throw new Error('Error while recognizing touch event.');
                    }

                    var coords = {
                        x: touchEvent.clientX,
                        y: touchEvent.clientY
                    }
                }
                else{
                    throw new Error('Unrecognized event type ' + event.type);
                }
            }
            var transformedCoords = {
                x: (coords.x - args.draggie.pointerDownPoint.x) / scale,
                y: (coords.y - args.draggie.pointerDownPoint.y) / scale
            }

            args.draggie.dragPoint.x = transformedCoords.x;
            args.draggie.dragPoint.y = transformedCoords.y;
            args.draggie.lastCoords = transformedCoords;
        });
    },
    setDragEndListener: function(args){
        if(!args.draggie){
            throw new Error("Expected a Draggabilly instance.");
        }

        return args.draggie.on('dragEnd', function( event, pointer) {
            args.draggie.$element.css({
                'left': args.draggie.startPosition.x + 'px',
                'top': args.draggie.startPosition.y + 'px'
            }).addClass('transition');
        
            setTimeout(function(){
                args.draggie.$element.removeClass('transition');
            }, 550);

            /*setTimeout(function () {
                presentation.currentProps.hammerInstance.options.enable = true;
              }, 100);*/
        
            module.exports.currentProps.$stage.removeClass('grabbing');
            module.exports.currentProps.dragging = false;
        });
    },
    getDistance: function(args){
        if(!_.isObject(args)){
            throw new Error('Some arguments were expected.')
        }
        if(!args.element1 instanceof Element){
            throw new Error('An element was expected as property element1.');
        }
        if(!args.element2 instanceof Element){
            throw new Error('An element was expected as property element2');
        }

        var distance = mezr.distance(args.element2, args.element1);
        return distance < 3;
    },
    getOverlap: function(args){
        var self = this;
        if(!_.isObject(args)){
            throw new Error('Some arguments were expected.')
        }
        
        if(typeof args.originalGroup === 'undefined'){
            throw new Error('A group was expected in property originalGroup');
        }

        if(!args.originalGroup instanceof jQuery){
            throw new Error('A jQuery collection was expected.');
        }

        if(!args.originalGroup.length){
            throw new Error('Original group has no length. Elements are non-existent in DOM.');
        }

        if(typeof args.matchingGroup === 'undefined'){
            throw new Error('A group was expected in property matchingGroup');
        }

        if(!args.matchingGroup instanceof jQuery){
            throw new Error('A jQuery collection was expected.');
        }

        if(!args.matchingGroup.length){
            throw new Error('Original group has no length. Elements are non-existent in DOM.');
        }

        var overlapping = false;

        args.originalGroup.each(function(){
            var $el = $(this);
            args.matchingGroup.each(function(){
                var $_el = $(this);
                if(self.getDistance({
                    element1: $el.get(0),
                    element2: $_el.get(0)
                }) === true){
                    overlapping = {
                        $element1: $el,
                        $element2: $_el
                    }
                    return false;
                }
            })
            if(_.isObject(overlapping)){
                return false;
            }
        });

        return overlapping;
    }
};